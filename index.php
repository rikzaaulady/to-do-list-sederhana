<?php
include_once("koneksi.php");

if (isset($_POST['simpan'])) {
    if (isset($_POST['id'])) {
        $ubah = mysqli_query($mysqli, "UPDATE kegiatan SET 
                                        isi = '" . $_POST['isi'] . "',
                                        tgl_awal = '" . $_POST['tgl_awal'] . "',
                                        tgl_akhir = '" . $_POST['tgl_akhir'] . "'
                                        WHERE
                                        id = '" . $_POST['id'] . "'");
    } else {
        $tambah = mysqli_query($mysqli, "INSERT INTO kegiatan(isi,tgl_awal,tgl_akhir,status) 
                                        VALUES ( 
                                            '" . $_POST['isi'] . "',
                                            '" . $_POST['tgl_awal'] . "',
                                            '" . $_POST['tgl_akhir'] . "',
                                            '0'
                                            )");
    }

    echo "<script> 
            document.location='index.php';
            </script>";
}


if(isset($_GET['aksi'])){

    if ($_GET['aksi'] == "hapus") {
        $hapus= mysqli_query($mysqli,"DELETE FROM kegiatan 
        WHERE id ='".$_GET['id']."'");
    } else if ($_GET['aksi'] == 'ubah_status') {
        $ubah_status = mysqli_query($mysqli,"UPDATE kegiatan SET
                                    status = '".$_GET['status']."'
                                    WHERE
                                    id ='".$_GET ['id']."'");
    }

    echo "<script>
    document.location='index.php';
    </script>";
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>


</head>
<body>


    <h3>
        To Do List
        <small class="text-muted">
            Catat Semua Hal yang kamu kerjakan
        </small>
        <li></li>
    </h3>
        <form class="row" method="POST" action="" name="myform">
            <?php
                $isi='';
                $tgl_awal='';
                $tgl_akhir='';
                if(isset($_GET['id'])) {
                    $ambil= mysqli_query($mysqli,"SELECT * FROM kegiatan WHERE  id='".$_GET['id'] . "'");
                    while ($row = mysqli_fetch_array($ambil)) {
                        $isi=$row['isi'];
                        $tgl_awal=$row['tgl_akhir'];
                        $tgl_akhir=$row['tgl_akhir'];
                    }
                ?>
                <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
                <?php
                }
                ?>

            <div class="row">
                <div class="col">
                    <label for="inputasi" class="">Kegiatan</label>
                    <input type="text" class="form-control" name="isi" placeholder="kegiatan" value="<?= $isi ?>">
                </div>
                <div class="col">
                    <label for="inputtanggalawal" class="">Tanggal awal</label>
                    <input type="date" class="form-control" name="tgl_awal" placeholder=" tanggal awal" value="<?= $tgl_awal ?>">
                </div>
                <div class="col">
                    <label for="inputtanggalakhir" class="">Tanggal akhir</label>
                    <input type="date" class="form-control" name="tgl_akhir" placeholder=" tanggal akhir" value="<?= $tgl_akhir ?>">
                </div>

                <div class="col">
                    <button type="submit" class="btn btn-primary rounded-pill px-3" name="simpan">Simpan</button>
                </div>

            </div>        

        </form>

        <table class="table table-hover">

            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Kegiatan</th>
                    <th scope="col">Awal</th>
                    <th scope="col">Akhir</th>
                    <th scope="col">Status</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <tbody>
                
                    <?php
                    $result = mysqli_query($mysqli, "SELECT * FROM kegiatan ORDER BY status,tgl_awal");
                    $no=1;
                    while ($data = mysqli_fetch_array($result)) { ?>
                            <tr>
                                <th scope="row"><?php echo $no++ ?></th>
                                <td><?php echo $data['isi'] ?></td>
                                <td><?php echo $data['tgl_awal'] ?></td>
                                <td><?php echo $data['tgl_akhir'] ?></td>
                                <td>
                                    <?php
                                        if ($data['status'] == '1') {
                                        ?>
                                            <a class="btn btn-success rounded-pill px-3" type="button" 
                                            href="index.php?id=<?php echo $data['id'] ?>&aksi=ubah_status&status=0">
                                            Sudah
                                            </a>
                                        <?php

                                        } else { 
                                            
                                        ?>
                                            <a class="btn btn-warning rounded-pill px-3" type="button" 
                                            href="index.php?id=<?php echo $data['id'] ?>&aksi=ubah_status&status=1">
                                            Belum</a>
                                        <?php
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <a class="btn btn-info rounded-pill px-3" 
                                        href="index.php?id=<?php echo $data['id'] ?>">Ubah
                                        </a>
                                        <a class="btn btn-danger rounded-pill px-3" 
                                        href="index.php?id=<?php echo $data['id'] ?>&aksi=hapus">Hapus
                                        </a>
                                </td>
                            </tr>
                    <?php
                    }
                    ?>
                        
                    </tr>
            </tbody>
        
        </table>
        <script type="text/javascript">
            function validate(){

                if(document.myform.isi.value ==""){
                    alert("Silahkan lengkapi bagian tanggal awal");
                    document.myform.tgl_awal.focus();
                    return false;
                } else if (document.myform.tgl_awal.value == ""){
                    alert("SIlahkan lengkapi bagiain awal");
                    document.myform.tgl_awal.focus();
                    return false;
                } else{
                    return true;
                }
            }

        </script>
</body>
</html>